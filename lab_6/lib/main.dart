import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  String about= 'Platform (Website) ini merupakan sarana bagi orang-orang yang ingin berbagi cerita seputar Covid-19. Baik membagikan pengalaman jika pernah terjangkit covid-19 sebelumnya, pengalaman yang dirasakan ketika melakukan vaksin, serta hal-hal apapun yang merupakan dampak dari pandemi covid ini. Selain itu, platform ini juga menyediakan forum bagi pengguna yang memiliki pertanyaan seputar covid-19 dimana pertanyaan tersebut bisa dijawab oleh pengguna lain yang sekiranya tahu atau pernah mengalami apa yang ditanyakan tersebut.';


  void doNothing(){

  }
  @override
  Widget build(BuildContext context) {
    final ButtonStyle style =
        TextButton.styleFrom(primary: Theme.of(context).colorScheme.onPrimary);
    return MaterialApp(
      theme: new ThemeData(
        brightness: Brightness.light,
      ),
      home: Scaffold(
          backgroundColor: Colors.greenAccent,
          appBar: AppBar(
            backgroundColor: const Color(0xAAC1FFD7),
            title: Text(
              'CORUM',
              style: TextStyle(color: Colors.black),
            ),
            actions: <Widget>[
              TextButton(
                style: style,
                onPressed: () {},
                child: const Text('Event'),
              ),
              TextButton(
                style: style,
                onPressed: () {},
                child: const Text('Blog'),
              ),
              TextButton(
                style: style,
                onPressed: () {},
                child: const Text('Forum'),
              ),
            ],
          ),
          body: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            Container(
              child: Text(
                'Hello, Welcome to our website\nCORUM\nShare With Us',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                ),
              ),
              alignment: Alignment.center,
              width: 600,
              height: 200,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.greenAccent,
                ),
                color: Colors.greenAccent,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(20),
              ),
              padding: EdgeInsets.all(20.0),
            ),
            Container(
              child: Text(about,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w500,
                ),
              ),
              alignment: Alignment.center,
              width: 600,
              height: 200,
              decoration: BoxDecoration(
                border: Border.all(
                  color: const Color(0xFFC2FFD9),
                ),
                color: const Color(0xFFC2FFD9),
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(20),
              ),
              padding: EdgeInsets.only(left: 20, right: 20),
              margin: EdgeInsets.fromLTRB(10, 0, 10, 20),
            ),
            SizedBox(
                height: 100, //height of button
                width: 300, //width of button
                child: ElevatedButton(
                  onPressed: doNothing,
                  child: Text(
                    "Feedback Form", 
                    style: TextStyle(color: Colors.black, fontSize: 20)),
                      style: ElevatedButton.styleFrom(
                        primary: const Color(0xFFC2FFD9),
                        shape: RoundedRectangleBorder(
                            //to set border radius to button
                            borderRadius: BorderRadius.circular(20)),
                            shadowColor: Colors.black,
                      ),
                  //parameters of Button class
                )),
          ])),
    );
  }
}
