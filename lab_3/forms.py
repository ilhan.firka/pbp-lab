from django import forms
from lab_1.models import Friend

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'dob']

    error_messages = {
        'required' : 'Please Type'
    }
    
    input_attrs = {
        'name':{
            'type' : 'text',
            'placeholder' : 'Your Name'
        },
        'npm':{
            'type': 'text',
            'placeholder' : 'Your ID'
        },
        'dob':{
            'type': 'date'
        }

    }
    name = forms.CharField(required=False, max_length=30, widget=forms.TextInput(attrs=input_attrs['name']))
    npm = forms.CharField(required=False, max_length=30, widget=forms.TextInput(attrs=input_attrs['npm']))
    dob = forms.DateField(required=False, widget=forms.TextInput(attrs=input_attrs['dob']))



    # name = forms.CharField(required=False, widget=input_attrs['name'])
    # npm = forms.CharField(required=False, widget=input_attrs['npm'])
    # dob = forms.DateField(required=False, widget=input_attrs['dob'])