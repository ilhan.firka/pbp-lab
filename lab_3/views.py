from django.forms.widgets import Input
from django.http import response
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required
# Create your views here.
@login_required(login_url='/admin/login/')

def index(request):
    # mengambil semua objek yang ada pada class Friend dan disimpan pada friends
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

def add_friend(request):
    form = FriendForm(request.POST or None)
    if(form.is_valid and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/lab-3')
    response = {"friendform" : form}
    return render(request, 'lab3_form.html', response)

