**1. Apakah perbedaan antara JSON dan XML?**<br>
XML merupakan markup language yang berfungsi untuk menyimpan dan mentransfer data dalam format yang terstruktur sehingga dapat dibaca oleh manusia. JSON merupakan sebuah format dalam pertukaran data yang menggunakan sintaks Javascript. Singkatnya, XML berorientasi pada dokumen sedangkan JSON berorientasi pada data.<br>

**2. Apakah perbedaan antara HTML dan XML?**<br>
XML berfokus pada transfer data sedangkan HTML berfokus pada penyajian data. Karena hal tersebut, HMTL bersifat statis dan XML bersifat dinamis.