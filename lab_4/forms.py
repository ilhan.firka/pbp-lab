from django import forms
from django.forms import fields, widgets
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['to', 'fromm', 'title', 'message']
    
    error_messages = {
        'required' : 'Please Type'
    }

    input_attrs = {
        'to':{
            'type' : 'text',
            'class':'form-control',
            'placeholder':'Tell me who do you want to send the message with',
        },
        'fromm':{
            'type' : 'text',
            'class':'form-control', 
            'placeholder':'From who'
        },
        'title':{
            'type' : 'text',
            'class':'form-control', 
            'placeholder':'What\'s the title'
        },
        'message':{
            'type' : 'text',
            'class':'form-control', 
            'placeholder':'WRITE THE MESSAGES'
        },
    }

    to = forms.CharField(required=False, widget=forms.TextInput(input_attrs['to']))
    fromm = forms.CharField(required=False, widget=forms.TextInput(input_attrs['fromm']))
    title = forms.CharField(required=False, widget=forms.TextInput(input_attrs['title']))
    message = forms.CharField(widget=forms.Textarea(input_attrs['message']))

